﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using System;
using System.Text;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using RabbitMQ.Client;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.RabbitMq
{
    public class RabbitMqService : IRabbitMqService
    {
        private readonly ConnectionFactory _connectionFactory;

        public RabbitMqService(IOptions<RabbitMqSettings> options)
        {
            _connectionFactory = new ConnectionFactory()
            {
                HostName = options.Value.HostName, 
                UserName = options.Value.UserName,
                Password = options.Value.Password
            };
        }

        public void GivePromoCodeToCustomerAsync(PromoCode promoCode)
        {
            using (var connection = _connectionFactory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                var dto = new GivePromoCodeToCustomerDto()
                {
                    PartnerId = promoCode.Partner.Id,
                    BeginDate = promoCode.BeginDate.ToShortDateString(),
                    EndDate = promoCode.EndDate.ToShortDateString(),
                    PreferenceId = promoCode.PreferenceId,
                    PromoCode = promoCode.Code,
                    ServiceInfo = promoCode.ServiceInfo,
                    PartnerManagerId = promoCode.PartnerManagerId
                };

                var json = JsonConvert.SerializeObject(dto);
                var body = Encoding.UTF8.GetBytes(json);

                channel.BasicPublish(exchange: "exchange.direct",
                    routingKey: "PromoCodeKey",
                    basicProperties: null,
                    body: body);

            }
        }

        public void NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId)
        {
            using (var connection = _connectionFactory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                var json = JsonConvert.SerializeObject(partnerManagerId);
                var body = Encoding.UTF8.GetBytes(json);

                channel.BasicPublish(exchange: "exchange.direct",
                    routingKey: "EmployeeKey",
                    basicProperties: null,
                    body: body);

            }
        }
    }
}
