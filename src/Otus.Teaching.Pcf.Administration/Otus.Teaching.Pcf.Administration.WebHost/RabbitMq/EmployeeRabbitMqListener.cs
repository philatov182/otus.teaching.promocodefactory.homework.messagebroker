﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.Administration.WebHost.Services;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Otus.Teaching.Pcf.Administration.WebHost.RabbitMq
{
    public class EmployeeRabbitMqListener : BackgroundService
    {
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly string _queueName;

        public EmployeeRabbitMqListener(IServiceScopeFactory scopeFactory, IOptions<RabbitMqSettings> options)
        {
            _scopeFactory = scopeFactory;
            _queueName = options.Value.QueueName;

            var exchange = "exchange.direct";

            var factory = new ConnectionFactory()
            {
                HostName = options.Value.HostName,
                UserName = options.Value.UserName,
                Password = options.Value.Password
            };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.BasicQos(0, 10, false);
            _channel.QueueDeclare(queue: _queueName, durable: false, exclusive: false, autoDelete: false, arguments: null);
            _channel.ExchangeDeclare(exchange, ExchangeType.Direct);
            _channel.QueueBind(_queueName, exchange, "EmployeeKey", null);
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += async (sender, e) =>
            {
                var content = Encoding.UTF8.GetString(e.Body.ToArray());
                var employeeId = JsonConvert.DeserializeObject<Guid>(content);

                using (var scope = _scopeFactory.CreateScope())
                {
                    var employeeService = scope.ServiceProvider.GetRequiredService<EmployeeService>();
                    await employeeService.UpdateAppliedPromocodesAsync(employeeId);
                }
            };

            _channel.BasicConsume(_queueName, true, consumer);

            return Task.CompletedTask;
        }

        public override void Dispose()
        {
            _channel.Close();
            _connection.Close();
            base.Dispose();
        }
    }
}
